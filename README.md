# Relayr Onboarding Python Test

### Requirements

* Python3
* pytest

### Run

```
$ python3 main.py

Input: ['asd', 'asdd', 'fre', 'glk', 'lkm'] looking for: 'sad'
Output: ['asd']
```

### Run tests

To run unittest will be necessary to install `pytest`.

```
$ python3 -m venv venv
$ . venv/bin/activate
$ pip3 install -r requirements.txt
$ python3 -m pytest tests
```

I chose pytest because:

* It is really simple to use, with easy synthax;
* It is well documented framework and widely used in the community;
* Supports parameterization, I used in this exercise
* Fixtures are simple to use

### License

```
/*
 * "THE BARBECUE-WARE LICENSE" (Revision 1):
 *
 * <benatto@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can make me a brazilian barbecue, including beers
 * and caipirinha in return to Paulo Leonardo Benatto.
 *
 * The quality of the barbecue depends on the amount of beer that has been
 * purchased.
 */
```
