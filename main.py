from finder import Finder


if __name__ == "__main__":
    words = ['asd', 'asdd', 'fre', 'glk', 'lkm']
    word = "sad"
    print(f"Input: {words} looking for: '{word}'")
    result = Finder(words).find(word)
    print(f"Output: {result}")
