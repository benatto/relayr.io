# Relayr Onboarding Big Data Deep-Dive

The purpose of this assignment is to help us understand your perspective on big data
systems, ahead of the technical interviews. To achieve this, you are tasked with creating a
high-level diagram of a big data system that solves the challenge of applying sentiment
analysis to a real-time stream of tweets.

## Assumptions

* the sentiment analysis model is pre-trained and available
* sentiment analysis models are available for all languages
* you have direct access to the twitter firehose, outputting ~6k tweets per second
* there is an SDK available that allows you to connect to the firehose


## Requirements

* the system is real-time
* the system has a way to cope with backpressure
* the system routes tweets to the correct model depending on language
* the system performs basic feature engineering as follows:
  * converting emoticons / smileys to tokens
  * converting GPS coordinates if available to City + Country
* the system saves data to the database system of your choice
* the system and the storage backend can scale horizontally

## Deliverables

* high-level description of the system (2-3 sentences)
* high-level system diagram (can be hand-drawn and photographed, no aesthetic
* requirements beyond readibility)
* description of libraries / frameworks chosen to solve the task (bullet points + 1 sentence for each suffice)
* any observations that you feel are relevant

## Notes

* Coding is not required
* logic such as routing can be expressed using boxes and arrows
* technology choices can be based on what's available or what you're familiar with
* there is generally no single right way to solve a task: focus on explaining your choices
* rather than finding the perfect tool

# Solution

I would say there is more than one approach for the problem of applying sentinment analysis
to a real-time stream of tweets. We could use:

1. Bare-metal solution with opensource tools/softwares.
2. Cloud-based solution using for example AWS or GCP.

## Generic Solution

* Develop an application using a general purpose programming language (Python, Go, Java) to connect
to firehose and feed the platform with all tweets through a Message Broker (RabbitMQ, Kafka and so on).
* Enricher services will collect raw message and enrich them, performing basic engineering tasks and
will push enchired messages to another queue.
* Routers services will collect the enriched message and route them to a specific queue based on the
language.
* Comprehend services will collect the messages from the queue based on the language, apply the sentiment
and send the final message to another queue.
* The keeper service will read the final enriched messages, with sentiment and save them into elastisearch.
* Probably the English language will have more tweets, so we could have more services reading from
the English queue.
* I will be honest, I never worked with elastic search, however for this problem it sounds a good solution
because it was built to scale. It is possible to perform and combine various kind of search (multi-language
and geo location), it is possible to use kibana to visualize the data.
* It is possible to find SDKs for the most used languages to connect to firehose,
pub/sub, connecting to message brokers, connect to elasticsearch.

![alt text](./arch.png "Solution")

## Cloud-based Solution

Based in the same idea in the Generic Solution we could use a lot of services in the cloud
to achieve the same results.

  * Amazon Kinesis Data Firehose
  * AWS SNS (pub/sub)
  * AWS Lambda Functions/Step Functions
  * Amazon ElasticSearch Service
  * Amazon Comprehend is a natural language processing (NLP) service that uses machine
    learning to find insights and relationships in text. No machine learning experience required.

## My Conclusion

* It is not easy to find/design the "right" solution based only on the description of a challenge.
The solution will be designed by the members of the team, working together to achieve the same goal,
sharing ideas and knowledge, looking the edge cases and black spots, analyzing pros and cons for
each possible idea/solution, trying to use the best tool/framework/language for the problem and
try to bring simplicity to a complex problem, and that my friend, is not something easy to achieve.
It can be hard, but it can be fun.
