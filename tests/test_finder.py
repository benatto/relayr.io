from finder import Finder

import pytest


@pytest.mark.parametrize(
    "list_words,word,expected",
    [
        (["asd"], "sad", ["asd"]),
        (["asd", "sad", "ads", "das"], "sad", ["asd", "sad", "ads", "das"]),
        (["Asd", "Sad", "Ads", "SAD"], "sad", []),
        ([], "sad", []),
        ([" "], "    ", []),
        ([""], "", [""]),
        ([" "], " ", [" "]),
        (["asd "], "sad", []),
        ([" asd"], "sad", []),
        (("tUple", "tuple"), "pletU", ["tUple"]),
        ({"Set", "SET"}, "SET", ["SET"]),
    ],
)
def test_finder(list_words, word, expected):
    assert Finder(list_words).find(word) == expected


@pytest.mark.parametrize(
    "list_words,word,expected",
    [
        ("asd", "sad", ["asd"]),
        ({}, "sad", []),
        (1, 1, [1])
    ]
)
def test_finder_with_invalid_input(list_words, word, expected):
    with pytest.raises(TypeError):
        Finder(list_words).find(word)


@pytest.mark.parametrize("word", [(1,), ([],), ({},)])
def test_find_with_invalid_word(word):
    with pytest.raises(TypeError):
        Finder([]).find(word)
