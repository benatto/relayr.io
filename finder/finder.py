import collections


class Finder(object):
    """
    The Finder object provides methods to search a word
    in a list of words.

    Args:
        words (list, tuple, set): The list of words

    Raises:
        TypeError: Invalid list of words
    """

    def __init__(self, words):
        if not isinstance(words, (list, tuple, set)):
            raise TypeError("Please provide a list, set or tuple.")
        self.words = words

    def find(self, word):
        """Find a word in a list of words. This method is
        case-sensitive.

        Args:
            word (str): The word to be searched.

        Returns:
            words (list): The list of words that matched the search.
        """

        if not isinstance(word, str):
            raise TypeError("Please provide a valid word.")

        return [
            w
            for w in self.words
            if (isinstance(w, str) and
                collections.Counter(word) == collections.Counter(w))
        ]
